//
//  PRListDomainTests.m
//  DesafioConcrete
//
//  Created by Roger dos Santos Oliveira on 11/11/16.
//  Copyright © 2016 RogerSanoli. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "RepoDomainFactory.h"

@interface PRListDomainTests : XCTestCase
@property (nonatomic, strong) id<PRDomainProtocol> pullRequest;
@end

@implementation PRListDomainTests

- (void)setUp {
    [super setUp];
    self.pullRequest = [RepoDomainFactory generatePRDomain];
}

- (void)tearDown {
    [super tearDown];
}

- (void)testState {
    [self.pullRequest prepareForTestWithState:@"open"];
    XCTAssertTrue([self.pullRequest isOpen], @"pull request should be open.");
    
    [self.pullRequest prepareForTestWithState:@"closed"];
    XCTAssertFalse([self.pullRequest isOpen], @"pull request should be closed.");
    
    [self.pullRequest prepareForTestWithState:@"batata"];
    XCTAssertFalse([self.pullRequest isOpen], @"pull request should be closed.");
    
    [self.pullRequest prepareForTestWithState:nil];
    XCTAssertFalse([self.pullRequest isOpen], @"pull request should be closed.");
}

- (void)testDate {
    [self.pullRequest prepareForTestWithDate:@"2016-11-11T19:47:52Z"];
    XCTAssertTrue([[self.pullRequest date] isEqualToString: @"11/11/2016 19:47:52"], @"The date must be successfully converted.");
    
    [self.pullRequest prepareForTestWithDate:@"2016.11.11 19:47:52Z"];
    XCTAssertTrue([[self.pullRequest date] isEqualToString: @""], @"Date in wrong format, should return empty string");
    
    [self.pullRequest prepareForTestWithDate:nil];
    XCTAssertTrue([[self.pullRequest date] isEqualToString: @""], @"Date is null, should return empty string");
}


@end
