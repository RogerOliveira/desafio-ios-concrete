//
//  RepoListTests.m
//  DesafioConcrete
//
//  Created by Roger dos Santos Oliveira on 11/11/16.
//  Copyright © 2016 RogerSanoli. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "RepoRepositoryFactory.h"

#define TIMEOUT 10.0f

@interface RepoListTests : XCTestCase
@property (nonatomic, strong) id<RepoListRepositoryProtocol> repository;
@end

@implementation RepoListTests

- (void)setUp {
    [super setUp];
    self.repository = [RepoRepositoryFactory generateRepoListRepository];
}

- (void)tearDown {
    [super tearDown];
}

- (void) testRepositoryCall {
    __weak XCTestExpectation *expectation = [self expectationWithDescription:@"Repository should be called."];
    
    [self.repository loadRepositoriesForPage:0 success:^(id<RepoListDomainProtocol> repoList) {

        XCTAssertNotNil(repoList, @"repoList cannot be nil.");
        XCTAssertNotNil([repoList items], @"items cannot be nil.");
        
        if([repoList items].count > 0) {
            id<RepoItemDomainProtocol> currentItem = [[repoList items] objectAtIndex:0];
            XCTAssertNotNil(currentItem, @"this item should not be nil.");
            XCTAssertNotNil([currentItem repoName], @"the repository must have a name.");
            id<RepoUserDomainProtocol> currentAuthor = [currentItem author];
            XCTAssertNotNil(currentAuthor, @"the repository must have an author.");
            XCTAssertNotNil([currentAuthor name], @"the author must have a name.");
        }

        [expectation fulfill];
        
    } failure:^(NSString *error) {
        XCTAssertNotNil(error, @"Error cannot be nil.");
        XCTFail(@"Error calling repository: %@",error);
        [expectation fulfill];
    }];
    
    [self waitForExpectationsWithTimeout:TIMEOUT handler:^(NSError *error) {
        if(error)
            XCTFail(@"The request timed out. Error: %@", error);
    }];

}

@end
