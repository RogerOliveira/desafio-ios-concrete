//
//  RepoManagerTests.m
//  DesafioConcrete
//
//  Created by Roger dos Santos Oliveira on 12/11/16.
//  Copyright © 2016 RogerSanoli. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "RepoManager.h"

#define TIMEOUT 20.0f

@interface RepoManagerTests : XCTestCase
@property (nonatomic, strong) RepoManager* repoManager;
@end

@implementation RepoManagerTests

- (void)setUp {
    [super setUp];
    self.repoManager = [[RepoManager alloc] init];
}

- (void)tearDown {
    [super tearDown];
}

- (void) testRepoManagerNextPage {
    __weak XCTestExpectation *expectation = [self expectationWithDescription:@"Paging should load more items."];
    
    [self.repoManager loadNext:^(NSArray<id<RepoItemDomainProtocol>> *itemList, BOOL hasMore) {
    
        XCTAssertNotNil(itemList, @"Item list cannot be nil, even if it is empty.");
        NSInteger firstCount = [itemList count];
        
        if(firstCount > 0) {
            [self.repoManager loadNext:^(NSArray<id<RepoItemDomainProtocol>> *itemList, BOOL hasMore) {
                XCTAssertNotNil(itemList, @"Item list cannot be nil, even if it is empty.");
                NSInteger secondCount = [itemList count];
                XCTAssertGreaterThan(secondCount, firstCount, @"Second call should have more items than the first one.");
                [expectation fulfill];
                
            } failure:^(NSString *error) {
                [self manageError:error expectation:expectation];
            }];
        }
        else {
            [expectation fulfill];
        }
    } failure:^(NSString *error) {
        [self manageError:error expectation:expectation];
    }];
    
    [self waitForExpectationsWithTimeout:TIMEOUT handler:^(NSError *error) {
        if(error)
            XCTFail(@"The request timed out. Error: %@", error);
    }];
}

- (void) testRepoManagerRefresh {
    __weak XCTestExpectation *expectation = [self expectationWithDescription:@"Refreshing should reload first page."];
    
    [self.repoManager loadNext:^(NSArray<id<RepoItemDomainProtocol>> *itemList, BOOL hasMore) {
        
        XCTAssertNotNil(itemList, @"Item list cannot be nil, even if it is empty.");
        NSInteger firstCount = [itemList count];
        
        if(firstCount > 0) {
            [self.repoManager refresh:^(NSArray<id<RepoItemDomainProtocol>> *itemList, BOOL hasMore) {
                
                XCTAssertNotNil(itemList, @"Item list cannot be nil, even if it is empty.");
                NSInteger secondCount = [itemList count];
                XCTAssertEqual(firstCount, secondCount, @"Second call should have the same amount of items of the first one.");
                
                [expectation fulfill];
                
            } failure:^(NSString *error) {
                [self manageError:error expectation:expectation];
            }];
        }
        else {
            [expectation fulfill];
        }
    } failure:^(NSString *error) {
        [self manageError:error expectation:expectation];
    }];
    
    [self waitForExpectationsWithTimeout:TIMEOUT handler:^(NSError *error) {
        if(error)
            XCTFail(@"The request timed out. Error: %@", error);
    }];
}


- (void) manageError: (NSString*) error expectation:(XCTestExpectation*) expectation {
    XCTAssertNotNil(error, @"Error cannot be nil.");
    [expectation fulfill];
    XCTFail(@"Error calling repo manager: %@",error);
}

@end
