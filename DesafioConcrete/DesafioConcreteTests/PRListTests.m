//
//  PRListTests.m
//  DesafioConcrete
//
//  Created by Roger dos Santos Oliveira on 11/11/16.
//  Copyright © 2016 RogerSanoli. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "RepoRepositoryFactory.h"
#import "RepoDomainFactory.h"

#define TIMEOUT 10.0f

@interface PRListTests : XCTestCase
@property (nonatomic, strong) id<PRRepositoryProtocol> repository;
@end

@implementation PRListTests

- (void)setUp {
    [super setUp];
    self.repository = [RepoRepositoryFactory generatePRRepository];
}

- (void)tearDown {
    [super tearDown];
}

- (void) testRepositoryCall {
    __weak XCTestExpectation *expectation = [self expectationWithDescription:@"Repository should be called."];
    
    id<RepoItemDomainProtocol> item = [RepoDomainFactory generateRepoItemDomain];
    [item prepareForTestWithRepoName:@"elasticsearch" authorName:@"elastic"];
    
    [self.repository loadPullRequestsForItem:item success:^(NSArray<id<PRDomainProtocol>> *prList) {
        XCTAssertNotNil(prList, @"repoList cannot be nil.");
        
        if([prList count] > 0) {
            id<PRDomainProtocol> currentPR = [prList objectAtIndex:0];
            XCTAssertNotNil(currentPR, @"this pull request should not be nil.");
            XCTAssertNotNil([currentPR title], @"the pull request must have a title.");
            id<RepoUserDomainProtocol> currentAuthor = [currentPR author];
            XCTAssertNotNil(currentAuthor, @"the pull request must have an author.");
            XCTAssertNotNil([currentAuthor name], @"the author must have a name.");
        }
        
        [expectation fulfill];
        
    } failure:^(NSString *error) {
        XCTAssertNotNil(error, @"Error cannot be nil.");
        [expectation fulfill];
        XCTFail(@"Error calling repository: %@",error);
    }];
    
    [self waitForExpectationsWithTimeout:TIMEOUT handler:^(NSError *error) {
        if(error)
            XCTFail(@"The request timed out. Error: %@", error);
    }];
    
}

@end
