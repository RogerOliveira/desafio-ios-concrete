//
//  AuthorView.m
//  DesafioConcrete
//
//  Created by Roger dos Santos Oliveira on 12/11/16.
//  Copyright © 2016 RogerSanoli. All rights reserved.
//

#import "AuthorView.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface AuthorView ()
@property (nonatomic, weak) IBOutlet UIImageView* photoView;
@property (nonatomic, weak) IBOutlet UILabel* nameLabel;
@property (nonatomic, weak) IBOutlet UILabel* fullNameLabel;
@end

@implementation AuthorView

- (instancetype) initWithType: (authorViewType) viewType {
    self = [[[NSBundle mainBundle] loadNibNamed:[self xibNameForType:viewType] owner:self options:nil] firstObject];
    
    if(self) {
        
    }
    
    return self;
}

- (NSString*) xibNameForType: (authorViewType) viewType {
    return viewType == authorViewTypeHorizontal? @"AuthorViewHorizontal" : @"AuthorViewVertical";
}

- (void) setup: (id<RepoUserDomainProtocol>) author {
    
    [self.photoView setImage:nil];
    
    self.photoView.layer.masksToBounds = YES;
    self.photoView.layer.cornerRadius = self.photoView.frame.size.height/2;
    
    [self.photoView sd_setImageWithURL:[NSURL URLWithString:[author photoURL]] placeholderImage:nil];
    self.nameLabel.text = [author name];
    self.fullNameLabel.text = [author fullName];
}


@end
