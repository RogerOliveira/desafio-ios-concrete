
//
//  RepoUserDomainProtocol.h
//  DesafioConcrete
//
//  Created by Roger dos Santos Oliveira on 11/11/16.
//  Copyright © 2016 RogerSanoli. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol RepoUserDomainProtocol <NSObject>
- (NSString*) photoURL;
- (NSString*) name;
- (NSString*) fullName;

- (void) prepareForTestWithName: (NSString*) authorName;
@end
