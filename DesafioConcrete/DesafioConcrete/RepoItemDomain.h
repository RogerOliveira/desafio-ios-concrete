//
//  RepoItemDomain.h
//  DesafioConcrete
//
//  Created by Roger dos Santos Oliveira on 11/11/16.
//  Copyright © 2016 RogerSanoli. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MTLModel.h"
#import "MTLJSONAdapter.h"
#import "NSValueTransformer+MTLPredefinedTransformerAdditions.h"
#import "RepoItemDomainProtocol.h"

@interface RepoItemDomain : MTLModel <RepoItemDomainProtocol, MTLJSONSerializing>

@property (nonatomic, strong) NSString* repoName;
@property (nonatomic, strong) NSString* repoDescription;
@property (nonatomic, readwrite) NSInteger forksCount;
@property (nonatomic, readwrite) NSInteger starsCount;
@property (nonatomic, strong) id<RepoUserDomainProtocol> author;

@end
