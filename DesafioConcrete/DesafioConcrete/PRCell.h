//
//  PRCell.h
//  DesafioConcrete
//
//  Created by Roger dos Santos Oliveira on 12/11/16.
//  Copyright © 2016 RogerSanoli. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PRDomainProtocol.h"
@interface PRCell : UITableViewCell

- (void) setup: (id<PRDomainProtocol>) pullRequest;

@end
