//
//  RepoUserDomain.m
//  DesafioConcrete
//
//  Created by Roger dos Santos Oliveira on 11/11/16.
//  Copyright © 2016 RogerSanoli. All rights reserved.
//

#import "RepoUserDomain.h"

@implementation RepoUserDomain

#pragma mark - JSON serializing
+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{
             @"photoURL" : @"avatar_url",
             @"name" : @"login",
             @"fullName" : @"login",
             };
}

#pragma mark - Unit Testing
- (void) prepareForTestWithName: (NSString*) authorName {
    self.name = authorName;
}
@end
