//
//  RepoDomainFactory.m
//  DesafioConcrete
//
//  Created by Roger dos Santos Oliveira on 11/11/16.
//  Copyright © 2016 RogerSanoli. All rights reserved.
//

#import "RepoDomainFactory.h"
#import "RepoListDomain.h"
#import "RepoItemDomain.h"
#import "RepoUserDomain.h"
#import "PRDomain.h"

@implementation RepoDomainFactory

+ (id<RepoListDomainProtocol>) generateRepoDomain {
    return [[RepoListDomain alloc] init];
}

+ (id<RepoItemDomainProtocol>) generateRepoItemDomain {
    return [[RepoItemDomain alloc] init];
}

+ (id<RepoUserDomainProtocol>) generateUserDomain {
    return [[RepoUserDomain alloc] init];
}

+ (id<PRDomainProtocol>) generatePRDomain {
    return [[PRDomain alloc] init];
}
@end
