//
//  AppDelegate.h
//  DesafioConcrete
//
//  Created by Roger dos Santos Oliveira on 10/11/16.
//  Copyright © 2016 RogerSanoli. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

