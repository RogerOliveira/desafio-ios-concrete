//
//  PRDomain.m
//  DesafioConcrete
//
//  Created by Roger dos Santos Oliveira on 11/11/16.
//  Copyright © 2016 RogerSanoli. All rights reserved.
//

#import "PRDomain.h"
#import "RepoDomainFactory.h"

#define OPEN @"open"

@interface PRDomain ()
@property (nonatomic, strong) NSString* creation_date;
@property (nonatomic, strong) NSString* state;
@end

@implementation PRDomain

#pragma mark - JSON serializing
+ (NSDictionary*)JSONKeyPathsByPropertyKey
{
    return @{
             @"title" : @"title",
             @"body" : @"body",
             @"htmlURL" : @"html_url",
             @"state" : @"state",
             @"creation_date" : @"created_at",
             @"author" : @"user"
             };
}

+ (NSValueTransformer *)authorJSONTransformer
{
    id<RepoUserDomainProtocol> user = [RepoDomainFactory generateUserDomain];
    return [MTLJSONAdapter dictionaryTransformerWithModelClass:[user class]];;
}

#pragma mark - Domain mathods
- (NSString*) date {
    if(nil == self.creation_date)
        return @"";

    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
    NSDate* currentDate = [formatter dateFromString:self.creation_date];
    [formatter setDateFormat:@"dd/MM/yyyy HH:mm:ss"];
    NSString* formattedDate = [formatter stringFromDate:currentDate];
    return nil == formattedDate? @"" : formattedDate;
}

- (BOOL) isOpen {
    return nil == self.state? NO : [self.state isEqualToString:OPEN];
}

#pragma mark - Unit Testing
- (void) prepareForTestWithDate: (NSString*) date {
    self.creation_date = date;
}

- (void) prepareForTestWithState: (NSString*) state {
    self.state = state;
}

@end
