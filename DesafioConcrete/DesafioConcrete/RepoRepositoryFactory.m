//
//  RepoRepositoryFactory.m
//  DesafioConcrete
//
//  Created by Roger dos Santos Oliveira on 11/11/16.
//  Copyright © 2016 RogerSanoli. All rights reserved.
//

#import "RepoRepositoryFactory.h"
#import "RepoListRepository.h"
#import "PRRepository.h"

@implementation RepoRepositoryFactory

+ (id<RepoListRepositoryProtocol>) generateRepoListRepository {
    return [[RepoListRepository alloc] init];
}

+ (id<PRRepositoryProtocol>) generatePRRepository {
    return [[PRRepository alloc] init];
}

@end
