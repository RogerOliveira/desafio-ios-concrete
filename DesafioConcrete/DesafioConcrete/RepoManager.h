//
//  RepoManager.h
//  DesafioConcrete
//
//  Created by Roger dos Santos Oliveira on 12/11/16.
//  Copyright © 2016 RogerSanoli. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RepoRepositoryFactory.h"

@interface RepoManager : NSObject

typedef void(^successRepoManager)(NSArray<id<RepoItemDomainProtocol>>* itemList, BOOL hasMore);
typedef void(^failureRepoManager)(NSString* error);

- (void) refresh:(successRepoManager) success failure:(failureRepoManager)failure;
- (void) loadNext:(successRepoManager) success failure:(failureRepoManager)failure;

@end
