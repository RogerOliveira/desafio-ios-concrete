//
//  PRRepository.h
//  DesafioConcrete
//
//  Created by Roger dos Santos Oliveira on 11/11/16.
//  Copyright © 2016 RogerSanoli. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PRRepositoryProtocol.h"
#import <AFNetworking/AFNetworking.h>

@interface PRRepository : AFHTTPSessionManager <PRRepositoryProtocol>

@end
