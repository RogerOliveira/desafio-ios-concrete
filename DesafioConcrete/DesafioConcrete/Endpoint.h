//
//  Endpoint.h
//  DesafioConcrete
//
//  Created by Roger dos Santos Oliveira on 11/11/16.
//  Copyright © 2016 RogerSanoli. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Endpoint : NSObject
+ (NSString*) BASE_URL;
@end
