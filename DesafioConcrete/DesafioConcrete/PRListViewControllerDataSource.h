//
//  PRListViewControllerDataSource.h
//  DesafioConcrete
//
//  Created by Roger dos Santos Oliveira on 13/11/16.
//  Copyright © 2016 RogerSanoli. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PRDomainProtocol.h"
#import "PRListViewControllerDataSourceDelegate.h"

@interface PRListViewControllerDataSource : NSObject <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, weak) id<PRListViewControllerDataSourceDelegate> delegate;

- (void) updateDataSource: (NSArray<id<PRDomainProtocol>>*) prList;
@end


