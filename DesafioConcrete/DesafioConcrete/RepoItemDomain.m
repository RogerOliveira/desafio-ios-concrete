//
//  RepoItemDomain.m
//  DesafioConcrete
//
//  Created by Roger dos Santos Oliveira on 11/11/16.
//  Copyright © 2016 RogerSanoli. All rights reserved.
//

#import "RepoItemDomain.h"
#import "RepoDomainFactory.h"

@implementation RepoItemDomain

#pragma mark - JSON serializing
+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{
             @"repoName" : @"name",
             @"repoDescription" : @"description",
             @"forksCount" : @"forks_count",
             @"starsCount" : @"watchers_count",
             @"author" : @"owner"
             };
}

+ (NSValueTransformer *)authorJSONTransformer
{
    id<RepoUserDomainProtocol> user = [RepoDomainFactory generateUserDomain];
    return [MTLJSONAdapter dictionaryTransformerWithModelClass:[user class]];;
}

#pragma mark - Unit Testing
- (void) prepareForTestWithRepoName: (NSString*) repoName authorName:(NSString*)authorName {
    self.repoName = repoName;
    self.author = [RepoDomainFactory generateUserDomain];
    [self.author prepareForTestWithName:authorName];
}

@end
