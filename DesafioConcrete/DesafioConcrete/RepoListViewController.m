//
//  RepoListViewController.m
//  DesafioConcrete
//
//  Created by Roger dos Santos Oliveira on 10/11/16.
//  Copyright © 2016 RogerSanoli. All rights reserved.
//

#import "RepoListViewController.h"
#import "RepoListViewControllerDataSource.h"
#import "RepoManager.h"
#import "UIColor+ColorHelper.h"
#import "PRListViewController.h"

#define VIEW_CONTROLLER_TITLE @"Github JavaPop"
#define ROW_HEIGHT 113.0f

@interface RepoListViewController () <RepoListViewControllerDataSourceDelegate, UIAlertViewDelegate>

#pragma mark - IBOutlets & components
@property (nonatomic, weak) IBOutlet UITableView* tableView;
@property (nonatomic, strong) UIRefreshControl* refreshControl;
@property (nonatomic, weak) IBOutlet UIActivityIndicatorView* loading;
#pragma mark - Data properties
@property (nonatomic, strong) RepoManager* repoManager;
@property (nonatomic, strong) RepoListViewControllerDataSource* dataSource;
@property (nonatomic, readwrite) BOOL isLoading;
@end

@implementation RepoListViewController

#pragma mark - Lifecycle & initial setup

- (instancetype) initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        self.isLoading = NO;
        self.repoManager = [[RepoManager alloc] init];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = VIEW_CONTROLLER_TITLE;
    
    [self setupTableView];
    [self setupRefreshControl];
    [self reload];
}

- (void) setupTableView {
    self.tableView.alpha = 0;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = ROW_HEIGHT;
    self.dataSource = [[RepoListViewControllerDataSource alloc] init];
    self.dataSource.delegate = self;
    self.tableView.dataSource = self.dataSource;
    self.tableView.delegate = self.dataSource;
}

- (void) setupRefreshControl {
    [self setRefreshControl:[[UIRefreshControl alloc] init]];
    self.refreshControl.tintColor = [UIColor r:52 g:52 b:56];
    [self.refreshControl addTarget:self action:@selector(reload) forControlEvents:UIControlEventValueChanged];
    [self.tableView insertSubview:self.refreshControl atIndex:0];
}


#pragma mark - DataSource delegate & Paging control
- (void) loadNextPage {
    
    if(self.isLoading)
        return;
    
    self.isLoading = YES;
    
    [self.repoManager loadNext:^(NSArray<id<RepoItemDomainProtocol>> *itemList, BOOL hasMore) {
        
        self.isLoading = NO;
        [self.dataSource updateDataSource:itemList hasMore:hasMore];
        [self.tableView reloadData];
        
    } failure:^(NSString *error) {
       
        self.isLoading = NO;
        [self showErrorAlert];
        
    }];
}

- (void) reload {
    
    if(self.isLoading)
        return;
    
    self.isLoading = YES;
    
    [self.dataSource updateDataSource:nil hasMore:YES];
    [self.tableView reloadData];
    
    [self.repoManager refresh:^(NSArray<id<RepoItemDomainProtocol>> *itemList, BOOL hasMore) {
        
        self.isLoading = NO;
        [self hideLoading];
        [self.refreshControl endRefreshing];
        [self.dataSource updateDataSource:itemList hasMore:hasMore];
        [self.tableView reloadData];
        
    } failure:^(NSString *error) {
        
        self.isLoading = NO;
        [self showErrorAlert];
        
    }];
}

- (void) loadRepositoriesForItem:(id<RepoItemDomainProtocol>)item {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    PRListViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"PRListViewController"];
    vc.repoItem = item;
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - Layout handling
- (void) hideLoading {
    if(self.tableView.alpha == 0) {
        self.tableView.alpha = 1;
        self.loading.alpha = 0;
    }
}

#pragma mark AlertView handling
- (void) showErrorAlert {
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"Erro"
                                  message:@"Não foi possível buscar repositórios, gostaria de tentar novamente?"
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* sim = [UIAlertAction
                          actionWithTitle:@"sim"
                          style:UIAlertActionStyleDefault
                          handler:^(UIAlertAction * action)
                          {
                              [alert dismissViewControllerAnimated:YES completion:nil];
                              [self loadNextPage];
                          }];
    
    UIAlertAction* nao = [UIAlertAction
                          actionWithTitle:@"não"
                          style:UIAlertActionStyleCancel
                          handler:^(UIAlertAction * action)
                          {
                              [alert dismissViewControllerAnimated:YES completion:nil];
                          }];
    
    [alert addAction:sim];
    [alert addAction:nao];
    
    [self presentViewController:alert animated:YES completion:nil];
}

@end
