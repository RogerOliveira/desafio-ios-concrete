//
//  RepoManager.m
//  DesafioConcrete
//
//  Created by Roger dos Santos Oliveira on 12/11/16.
//  Copyright © 2016 RogerSanoli. All rights reserved.
//

#import "RepoManager.h"

@interface RepoManager ()
@property (nonatomic, readwrite) int currentPage;
@property (nonatomic, readwrite) BOOL hasMore;
@property (nonatomic, strong) NSMutableArray<id<RepoItemDomainProtocol>>* items;
@end

@implementation RepoManager

- (instancetype) init{
    if(self = [super init]) {
        self.currentPage = 1;
        self.hasMore = YES;
        self.items = [[NSMutableArray<id<RepoItemDomainProtocol>> alloc] init];
    }
    return self;
}

- (void) refresh:(successRepoManager) success failure:(failureRepoManager)failure {
    self.currentPage = 1;
    self.hasMore = YES;
    self.items = [[NSMutableArray<id<RepoItemDomainProtocol>> alloc] init];
    [self loadNext: success failure:failure];
}

- (void) loadNext:(successRepoManager) success failure:(failureRepoManager)failure {
    id<RepoListRepositoryProtocol> repository = [RepoRepositoryFactory generateRepoListRepository];
    [repository loadRepositoriesForPage:self.currentPage success:^(id<RepoListDomainProtocol> repoList) {
        if(nil != [repoList items]) {
            [self.items addObjectsFromArray:[repoList items]];
            self.hasMore = [self.items count]<[repoList repoCount];
            self.currentPage++;
        }
        else {
            self.hasMore = NO;
        }
        
        success(self.items, self.hasMore);
        
    } failure:^(NSString *error) {
        failure(error);
    }];
}

@end
