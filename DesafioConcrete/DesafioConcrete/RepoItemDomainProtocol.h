//
//  RepoItemDomainProtocol.h
//  DesafioConcrete
//
//  Created by Roger dos Santos Oliveira on 11/11/16.
//  Copyright © 2016 RogerSanoli. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RepoUserDomainProtocol.h"

@protocol RepoItemDomainProtocol <NSObject>
- (NSString*) repoName;
- (NSString*) repoDescription;
- (NSInteger) forksCount;
- (NSInteger) starsCount;
- (id<RepoUserDomainProtocol>) author;

- (void) prepareForTestWithRepoName: (NSString*) repoName authorName:(NSString*)authorName;
@end
