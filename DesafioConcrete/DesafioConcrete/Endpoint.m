//
//  Endpoint.m
//  DesafioConcrete
//
//  Created by Roger dos Santos Oliveira on 11/11/16.
//  Copyright © 2016 RogerSanoli. All rights reserved.
//

#import "Endpoint.h"

@implementation Endpoint
+ (NSString*) BASE_URL {
    return @"https://api.github.com";
}
@end
