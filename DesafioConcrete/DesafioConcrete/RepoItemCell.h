//
//  RepoItemCell.h
//  DesafioConcrete
//
//  Created by Roger dos Santos Oliveira on 12/11/16.
//  Copyright © 2016 RogerSanoli. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RepoItemDomainProtocol.h"

@interface RepoItemCell : UITableViewCell

- (void) setup: (id<RepoItemDomainProtocol>) item;

@end
