//
//  PRListViewController.h
//  DesafioConcrete
//
//  Created by Roger dos Santos Oliveira on 10/11/16.
//  Copyright © 2016 RogerSanoli. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RepoItemDomainProtocol.h"
@interface PRListViewController : UIViewController

@property (nonatomic, strong) id<RepoItemDomainProtocol> repoItem;
@end
