//
//  PRRepository.m
//  DesafioConcrete
//
//  Created by Roger dos Santos Oliveira on 11/11/16.
//  Copyright © 2016 RogerSanoli. All rights reserved.
//

#import "PRRepository.h"
#import "RepoItemDomainProtocol.h"
#import "Endpoint.h"
#import "RepoDomainFactory.h"
#import "MTLJSONAdapter.h"

@implementation PRRepository

- (instancetype) init {
    if(self = [super initWithBaseURL:[NSURL URLWithString:[Endpoint BASE_URL]]]) {
        self.responseSerializer = [AFJSONResponseSerializer serializer];
        self.requestSerializer = [AFJSONRequestSerializer serializer];
    }
    return self;
}

- (void) loadPullRequestsForItem: (id<RepoItemDomainProtocol>) item success:(successPR) success failure:(failurePR)failure {
    NSString* path = [self generatePathForItem: item];
    
    [self GET:path parameters:nil progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSError *error;
        id<PRDomainProtocol> domain = [RepoDomainFactory generatePRDomain];
        NSArray<id<PRDomainProtocol>>* prList = [MTLJSONAdapter modelsOfClass:[domain class] fromJSONArray:responseObject error:&error];
        
        if(error) {
            failure([error localizedDescription]);
            return;
        }
        
        success(prList);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failure([error localizedDescription]);
    }];
}

- (NSString*) generatePathForItem: (id<RepoItemDomainProtocol>) item {
    return [NSString stringWithFormat:@"/repos/%@/%@/pulls", [[item author] name], [item repoName]];
}

@end
