//
//  PRRepositoryProtocol.h
//  DesafioConcrete
//
//  Created by Roger dos Santos Oliveira on 11/11/16.
//  Copyright © 2016 RogerSanoli. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PRDomainProtocol.h"

typedef void(^successPR)(NSArray<id<PRDomainProtocol>>* prList);
typedef void(^failurePR)(NSString* error);

@protocol RepoItemDomainProtocol;

@protocol PRRepositoryProtocol <NSObject>
- (void) loadPullRequestsForItem: (id<RepoItemDomainProtocol>) item success:(successPR) success failure:(failurePR)failure;
@end
