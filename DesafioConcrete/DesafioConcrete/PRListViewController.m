//
//  PRListViewController.m
//  DesafioConcrete
//
//  Created by Roger dos Santos Oliveira on 10/11/16.
//  Copyright © 2016 RogerSanoli. All rights reserved.
//

#import "PRListViewController.h"
#import "PRListViewControllerDataSource.h"
#import "RepoRepositoryFactory.h"
#import "PRListHeaderView.h"

#define ROW_HEIGHT 159.0f

@interface PRListViewController () <PRListViewControllerDataSourceDelegate, UIAlertViewDelegate>
#pragma mark - IBOutlets & components
@property (nonatomic, weak) IBOutlet UITableView* tableView;
@property (nonatomic, weak) IBOutlet UIActivityIndicatorView* loading;
@property (nonatomic, weak) IBOutlet UILabel* noResultsLabel;
#pragma mark - Data properties
@property (nonatomic, strong) PRListViewControllerDataSource* dataSource;
@end

@implementation PRListViewController

#pragma mark - Lifecycle & initial setup

- (instancetype) initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
    
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupTableView];
    [self loadPullRequests];
}

- (void) setupTableView {
    self.tableView.alpha = 0;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = ROW_HEIGHT;
    self.dataSource = [[PRListViewControllerDataSource alloc] init];
    self.dataSource.delegate = self;
    self.tableView.dataSource = self.dataSource;
    self.tableView.delegate = self.dataSource;
}

- (void) setupHeader: (NSArray<id<PRDomainProtocol>>*) prList {
    PRListHeaderView* header = [[PRListHeaderView alloc] initWithPRList:prList];
    [self.tableView setTableHeaderView:header];
}

#pragma mark - Repository calling
- (void) loadPullRequests {
    
    id<PRRepositoryProtocol> repository = [RepoRepositoryFactory generatePRRepository];
    
   [repository loadPullRequestsForItem:self.repoItem success:^(NSArray<id<PRDomainProtocol>> *prList) {
       [self.dataSource updateDataSource:prList];
       [self setupHeader:prList];
       [self.tableView reloadData];
       self.tableView.alpha = [prList count] > 0;
       self.noResultsLabel.alpha = [prList count] == 0;
       self.loading.alpha = 0;
   } failure:^(NSString *error) {
       [self showErrorAlert];
   }];
}

#pragma mark - DataSource Delegate
- (void) openURL:(NSString *)url {
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"Atenção!"
                                  message:@"Você será redirecionado para fora da aplicação. Deseja continuar?"
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* sim = [UIAlertAction
                          actionWithTitle:@"sim"
                          style:UIAlertActionStyleDefault
                          handler:^(UIAlertAction * action)
                          {
                              [alert dismissViewControllerAnimated:YES completion:nil];
                              [[UIApplication sharedApplication] openURL:[NSURL URLWithString: url]];
                          }];
    
    UIAlertAction* nao = [UIAlertAction
                          actionWithTitle:@"não"
                          style:UIAlertActionStyleCancel
                          handler:^(UIAlertAction * action)
                          {
                              [alert dismissViewControllerAnimated:YES completion:nil];
                          }];
    
    [alert addAction:sim];
    [alert addAction:nao];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self presentViewController:alert animated:YES completion:nil];
    });
}

#pragma mark AlertView handling
- (void) showErrorAlert {
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"Erro"
                                  message:@"Não foi possível buscar pull requests, gostaria de tentar novamente?"
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* sim = [UIAlertAction
                          actionWithTitle:@"sim"
                          style:UIAlertActionStyleDefault
                          handler:^(UIAlertAction * action)
                          {
                              [alert dismissViewControllerAnimated:YES completion:nil];
                              [self loadPullRequests];
                          }];
    
    UIAlertAction* nao = [UIAlertAction
                          actionWithTitle:@"não"
                          style:UIAlertActionStyleCancel
                          handler:^(UIAlertAction * action)
                          {
                              [alert dismissViewControllerAnimated:YES completion:nil];
                          }];
    
    [alert addAction:sim];
    [alert addAction:nao];
    
    [self presentViewController:alert animated:YES completion:nil];
}


@end
