//
//  RepoListRepositoryProtocol.h
//  DesafioConcrete
//
//  Created by Roger dos Santos Oliveira on 11/11/16.
//  Copyright © 2016 RogerSanoli. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RepoListDomainProtocol.h"

typedef void(^successRepoList)(id<RepoListDomainProtocol> repoList);
typedef void(^failureRepoList)(NSString* error);

@protocol RepoListRepositoryProtocol <NSObject>
- (void) loadRepositoriesForPage: (NSInteger) page success:(successRepoList) success failure:(failureRepoList)failure;
@end
