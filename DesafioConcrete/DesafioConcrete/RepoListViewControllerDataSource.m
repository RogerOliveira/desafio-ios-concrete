//
//  RepoListViewControllerDataSource.m
//  DesafioConcrete
//
//  Created by Roger dos Santos Oliveira on 13/11/16.
//  Copyright © 2016 RogerSanoli. All rights reserved.
//

#import "RepoListViewControllerDataSource.h"
#import "RepoItemCell.h"
#import "LoadingCell.h"

#define CELL_IDENTIFIER @"RepoListCell"
#define LOADING_CELL_IDENTIFIER @"LoadingCell"
#define PAGINATION_DISTANCE -100

@interface RepoListViewControllerDataSource ()
@property (nonatomic, strong) NSArray<id<RepoItemDomainProtocol>>* itemList;
@property (nonatomic, readwrite) BOOL hasMore;
@end

@implementation RepoListViewControllerDataSource

#pragma mark - Initial setup & update
- (instancetype) init {
    self = [super init];
    if(self) {
        self.hasMore = YES;
    }
    return self;
}

- (void) updateDataSource: (NSArray<id<RepoItemDomainProtocol>>*) itemList hasMore:(BOOL)hasMore{
    self.itemList = itemList;
    self.hasMore = hasMore;
}

#pragma mark TableView delegate & data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return nil == self.itemList? 0 : [self.itemList count] + (self.hasMore? 1 : 0);
}

- (UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath  {
    
    if(![self isLastCell:indexPath]) {
        RepoItemCell* cell = [tableView dequeueReusableCellWithIdentifier:CELL_IDENTIFIER];
        if(nil == cell) {
            cell = [[RepoItemCell alloc] init];
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        }
        
        id<RepoItemDomainProtocol> currentItem = [self.itemList objectAtIndex:[indexPath row]];
        [cell setup: currentItem];
        return cell;
    }
    else {
        LoadingCell* cell = [tableView dequeueReusableCellWithIdentifier:LOADING_CELL_IDENTIFIER];
        if(nil == cell) {
            cell = [[LoadingCell alloc] init];
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        }
        return cell;
    }
}

- (BOOL) isLastCell:(NSIndexPath *)indexPath {
    return self.hasMore && [indexPath row] == [self.itemList count];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if([self isLastCell:indexPath] || nil == self.delegate)
        return;
    
    id<RepoItemDomainProtocol> currentItem = [self.itemList objectAtIndex:[indexPath row]];
    [self.delegate loadRepositoriesForItem:currentItem];
}

- (void)scrollViewDidScroll:(UIScrollView *)aScrollView {
    [self checkPagination: aScrollView];
}

- (void) checkPagination:(UIScrollView *)aScrollView  {
    
    CGPoint offset = aScrollView.contentOffset;
    CGRect bounds = aScrollView.bounds;
    CGSize size = aScrollView.contentSize;
    UIEdgeInsets inset = aScrollView.contentInset;
    float y = offset.y + bounds.size.height - inset.bottom;
    float h = size.height;
    
    if(y > h + PAGINATION_DISTANCE) {
        if(self.hasMore) {
            if(nil != self.delegate)
                [self.delegate loadNextPage];
        }
    }
}

@end
