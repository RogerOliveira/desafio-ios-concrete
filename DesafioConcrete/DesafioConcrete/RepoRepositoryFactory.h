//
//  RepoRepositoryFactory.h
//  DesafioConcrete
//
//  Created by Roger dos Santos Oliveira on 11/11/16.
//  Copyright © 2016 RogerSanoli. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RepoListRepositoryProtocol.h"
#import "PRRepositoryProtocol.h"

@interface RepoRepositoryFactory : NSObject

+ (id<RepoListRepositoryProtocol>) generateRepoListRepository;
+ (id<PRRepositoryProtocol>) generatePRRepository;

@end
