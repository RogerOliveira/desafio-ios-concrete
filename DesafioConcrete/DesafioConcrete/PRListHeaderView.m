//
//  PRListHeaderView.m
//  DesafioConcrete
//
//  Created by Roger dos Santos Oliveira on 12/11/16.
//  Copyright © 2016 RogerSanoli. All rights reserved.
//

#import "PRListHeaderView.h"
#import "UIColor+ColorHelper.h"

@interface PRListHeaderView ()
@property (nonatomic, weak) IBOutlet UILabel* stateLabel;
@end

@implementation PRListHeaderView

- (instancetype) initWithPRList: (NSArray<id<PRDomainProtocol>>*) prList {
    self = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil] firstObject];
    
    if(self) {
        self.stateLabel.attributedText = [self generateTextFromPRList:prList];
    }
    
    return self;
}

- (NSAttributedString*) generateTextFromPRList: (NSArray<id<PRDomainProtocol>>*) prList {
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"isOpen == YES"];
    NSArray *openArray = [prList filteredArrayUsingPredicate:predicate];
    predicate = [NSPredicate predicateWithFormat:@"isOpen == NO"];
    NSArray *closedArray = [prList filteredArrayUsingPredicate:predicate];
    
    NSString* openStr = [NSString stringWithFormat:@"%li opened", [openArray count]];
    NSString* closedStr = [NSString stringWithFormat:@"/ %li closed", [closedArray count]];
    
    UIFont *font = [UIFont boldSystemFontOfSize:12.0f];
    
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setAlignment:NSTextAlignmentCenter];
    
    NSMutableAttributedString *stateStr = [[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"%@ %@", openStr, closedStr]];
    
    [stateStr beginEditing];
    
    [stateStr addAttribute:NSFontAttributeName value:font range:NSMakeRange(0, [stateStr length])];
    
    [stateStr addAttribute:NSForegroundColorAttributeName value:[UIColor r:226 g:160 b:49] range:NSMakeRange(0, [openStr length])];
    
    [stateStr addAttribute:NSForegroundColorAttributeName value:[UIColor r:52 g:52 b:56] range:NSMakeRange([openStr length], [closedStr length])];
    
    [stateStr addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [stateStr length])];
    
    [stateStr endEditing];
    
    return stateStr;
}

@end
