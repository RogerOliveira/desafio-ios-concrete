//
//  UIColor+ColorHelper.h
//  DesafioConcrete
//
//  Created by Roger dos Santos Oliveira on 12/11/16.
//  Copyright © 2016 RogerSanoli. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (ColorHelper)

+ (UIColor*) r:(NSInteger)r g:(NSInteger)g b:(NSInteger)b;
+ (UIColor*) r:(NSInteger)r g:(NSInteger)g b:(NSInteger)b a:(float) a;

@end
