//
//  RepoItemCell.m
//  DesafioConcrete
//
//  Created by Roger dos Santos Oliveira on 12/11/16.
//  Copyright © 2016 RogerSanoli. All rights reserved.
//

#import "RepoItemCell.h"
#import "AuthorView.h"

@interface RepoItemCell ()
@property (nonatomic, weak) IBOutlet UILabel* nameLabel;
@property (nonatomic, weak) IBOutlet UILabel* descriptionLabel;
@property (nonatomic, weak) IBOutlet UILabel* starLabel;
@property (nonatomic, weak) IBOutlet UILabel* forkLabel;
@property (nonatomic, weak) IBOutlet UIView* authorViewContainer;
@property (nonatomic, strong) AuthorView* authorView;
@end

@implementation RepoItemCell

- (instancetype) init {
    self = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil] firstObject];
    
    if(self) {
        
    }
    
    return self;
}

- (void) setup: (id<RepoItemDomainProtocol>) item {
    self.nameLabel.text = [item repoName];
    self.descriptionLabel.text = [item repoDescription];
    self.starLabel.text = [NSString stringWithFormat:@"%li", [item starsCount]];
    self.forkLabel.text = [NSString stringWithFormat:@"%li", [item forksCount]];
    self.authorView = [[AuthorView alloc] initWithType:authorViewTypeVertical];
    self.authorView.frame = CGRectMake(0, 0, self.authorViewContainer.frame.size.width, self.authorViewContainer.frame.size.height);
    [self.authorViewContainer addSubview:self.authorView];
    [self.authorView setup:[item author]];
}

@end
