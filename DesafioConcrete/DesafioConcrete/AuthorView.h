//
//  AuthorView.h
//  DesafioConcrete
//
//  Created by Roger dos Santos Oliveira on 12/11/16.
//  Copyright © 2016 RogerSanoli. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RepoUserDomainProtocol.h"

@interface AuthorView : UIView

typedef NS_ENUM(NSInteger, authorViewType) {
    authorViewTypeHorizontal,
    authorViewTypeVertical
};

- (instancetype) init __attribute__ ((unavailable("use initWithType: instead")));
- (instancetype) initWithType: (authorViewType) viewType;

- (void) setup: (id<RepoUserDomainProtocol>) author;

@end
