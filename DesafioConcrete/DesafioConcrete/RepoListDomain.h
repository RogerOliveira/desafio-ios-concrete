//
//  RepoListDomain.h
//  DesafioConcrete
//
//  Created by Roger dos Santos Oliveira on 11/11/16.
//  Copyright © 2016 RogerSanoli. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MTLModel.h"
#import "MTLJSONAdapter.h"
#import "NSValueTransformer+MTLPredefinedTransformerAdditions.h"
#import "RepoListDomainProtocol.h"

@interface RepoListDomain : MTLModel <RepoListDomainProtocol, MTLJSONSerializing>

@property (nonatomic, readwrite) NSInteger repoCount;
@property (nonatomic, strong) NSArray<id<RepoItemDomainProtocol>>* items;

@end
