//
//  PRDomain.h
//  DesafioConcrete
//
//  Created by Roger dos Santos Oliveira on 11/11/16.
//  Copyright © 2016 RogerSanoli. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MTLModel.h"
#import "MTLJSONAdapter.h"
#import "NSValueTransformer+MTLPredefinedTransformerAdditions.h"
#import "PRDomainProtocol.h"

@interface PRDomain : MTLModel <PRDomainProtocol, MTLJSONSerializing>

@property (nonatomic, strong) NSString* title;
@property (nonatomic, strong) NSString* body;
@property (nonatomic, strong) NSString* htmlURL;
@property (nonatomic, strong) id<RepoUserDomainProtocol> author;

@end
