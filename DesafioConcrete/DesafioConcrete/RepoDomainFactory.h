//
//  RepoDomainFactory.h
//  DesafioConcrete
//
//  Created by Roger dos Santos Oliveira on 11/11/16.
//  Copyright © 2016 RogerSanoli. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RepoListDomainProtocol.h"
#import "RepoItemDomainProtocol.h"
#import "RepoUserDomainProtocol.h"
#import "PRDomainProtocol.h"

@interface RepoDomainFactory : NSObject
+ (id<RepoListDomainProtocol>) generateRepoDomain;
+ (id<RepoItemDomainProtocol>) generateRepoItemDomain;
+ (id<RepoUserDomainProtocol>) generateUserDomain;
+ (id<PRDomainProtocol>) generatePRDomain;
@end
