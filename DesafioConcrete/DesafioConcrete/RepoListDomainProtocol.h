//
//  RepoListDomainProtocol.h
//  DesafioConcrete
//
//  Created by Roger dos Santos Oliveira on 11/11/16.
//  Copyright © 2016 RogerSanoli. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RepoItemDomainProtocol.h"

@protocol RepoListDomainProtocol <NSObject>
- (NSInteger) repoCount;
- (NSArray<id<RepoItemDomainProtocol>>*) items;
@end
