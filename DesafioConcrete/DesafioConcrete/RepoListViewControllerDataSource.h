//
//  RepoListViewControllerDataSource.h
//  DesafioConcrete
//
//  Created by Roger dos Santos Oliveira on 13/11/16.
//  Copyright © 2016 RogerSanoli. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RepoItemDomainProtocol.h"
#import "RepoListViewControllerDataSourceDelegate.h"

@interface RepoListViewControllerDataSource : NSObject <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, weak) id<RepoListViewControllerDataSourceDelegate> delegate;

- (void) updateDataSource: (NSArray<id<RepoItemDomainProtocol>>*) itemList hasMore:(BOOL)hasMore;

@end
