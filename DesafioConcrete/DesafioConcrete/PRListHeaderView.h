//
//  PRListHeaderView.h
//  DesafioConcrete
//
//  Created by Roger dos Santos Oliveira on 12/11/16.
//  Copyright © 2016 RogerSanoli. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PRDomainProtocol.h"

@interface PRListHeaderView : UIView

- (instancetype) init __attribute__ ((unavailable("use initWithPRList: instead")));
- (instancetype) initWithPRList: (NSArray<id<PRDomainProtocol>>*) prList;

@end
