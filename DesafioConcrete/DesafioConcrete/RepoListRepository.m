//
//  RepoListRepository.m
//  DesafioConcrete
//
//  Created by Roger dos Santos Oliveira on 11/11/16.
//  Copyright © 2016 RogerSanoli. All rights reserved.
//

#import "RepoListRepository.h"
#import "Endpoint.h"
#import "RepoDomainFactory.h"
#import "MTLJSONAdapter.h"

@implementation RepoListRepository

- (instancetype) init {
    if(self = [super initWithBaseURL:[NSURL URLWithString:[Endpoint BASE_URL]]]) {
        self.responseSerializer = [AFJSONResponseSerializer serializer];
        self.requestSerializer = [AFJSONRequestSerializer serializer];
    }
    return self;
}

- (void) loadRepositoriesForPage: (NSInteger) page success:(successRepoList) success failure:(failureRepoList)failure {
    NSString* path = [self generatePathForPage: page];
    
    [self GET:path parameters:nil progress:^(NSProgress * _Nonnull uploadProgress) {
    
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSError *error;
        id<RepoListDomainProtocol> domain = [RepoDomainFactory generateRepoDomain];
        domain = [MTLJSONAdapter modelOfClass:[domain class] fromJSONDictionary:responseObject error:&error];
        
        if(error) {
            failure([error localizedDescription]);
            return;
        }
        
        success(domain);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failure([error localizedDescription]);
    }];
}

- (NSString*) generatePathForPage: (NSInteger) page {
    return [NSString stringWithFormat:@"/search/repositories?q=language:Java&sort=stars&page=%li", page];
}
@end
