//
//  RepoList.m
//  DesafioConcrete
//
//  Created by Roger dos Santos Oliveira on 11/11/16.
//  Copyright © 2016 RogerSanoli. All rights reserved.
//

#import "RepoListDomain.h"
#import "RepoDomainFactory.h"

@implementation RepoListDomain

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{
             @"repoCount" : @"total_count",
             @"items" : @"items"
             };
}

+ (NSValueTransformer *)itemsJSONTransformer
{
    id<RepoItemDomainProtocol> item = [RepoDomainFactory generateRepoItemDomain];
    return [MTLJSONAdapter arrayTransformerWithModelClass:[item class]];;
}


@end
