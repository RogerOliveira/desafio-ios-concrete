//
//  RepoListViewControllerDataSourceDelegate.h
//  DesafioConcrete
//
//  Created by Roger dos Santos Oliveira on 13/11/16.
//  Copyright © 2016 RogerSanoli. All rights reserved.
//

#import <Foundation/Foundation.h>
@protocol RepoItemDomainProtocol;
@protocol RepoListViewControllerDataSourceDelegate <NSObject>
- (void) loadNextPage;
- (void) loadRepositoriesForItem:(id<RepoItemDomainProtocol>) item;
@end
