//
//  UIColor+ColorHelper.m
//  DesafioConcrete
//
//  Created by Roger dos Santos Oliveira on 12/11/16.
//  Copyright © 2016 RogerSanoli. All rights reserved.
//

#import "UIColor+ColorHelper.h"

@implementation UIColor (ColorHelper)

+ (UIColor*) r:(NSInteger)r g:(NSInteger)g b:(NSInteger)b {
    return [UIColor r:r g:g b:b a:1];
}

+ (UIColor*) r:(NSInteger)r g:(NSInteger)g b:(NSInteger)b a:(float) a {
    return [UIColor colorWithRed:r/255.0f green:g/255.0f blue:b/255.0f alpha:a];
}


@end
