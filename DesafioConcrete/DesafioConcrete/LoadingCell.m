//
//  LoadingCell.m
//  DesafioConcrete
//
//  Created by Roger dos Santos Oliveira on 13/11/16.
//  Copyright © 2016 RogerSanoli. All rights reserved.
//

#import "LoadingCell.h"

@implementation LoadingCell

- (instancetype) init {
    
    self = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil] firstObject];
    
    if(self) {
        
    }
    return self;
}

@end
