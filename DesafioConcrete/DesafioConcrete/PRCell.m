//
//  PRCell.m
//  DesafioConcrete
//
//  Created by Roger dos Santos Oliveira on 12/11/16.
//  Copyright © 2016 RogerSanoli. All rights reserved.
//

#import "PRCell.h"
#import "AuthorView.h"

@interface PRCell ()
@property (nonatomic, weak) IBOutlet UILabel* titleLabel;
@property (nonatomic, weak) IBOutlet UILabel* bodyLabel;
@property (nonatomic, weak) IBOutlet UILabel* dateLabel;
@property (nonatomic, weak) IBOutlet UIView* authorViewContainer;
@property (nonatomic, strong) AuthorView* authorView;
@end

@implementation PRCell

- (instancetype) init {
    self = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil] firstObject];
    
    if(self) {
       
    }
    
    return self;
}

- (void) setup: (id<PRDomainProtocol>) pullRequest {
    self.titleLabel.text = [pullRequest title];
    self.bodyLabel.text = [pullRequest body];
    self.dateLabel.text = [pullRequest date];
    self.authorView = [[AuthorView alloc] initWithType:authorViewTypeHorizontal];
    self.authorView.frame = CGRectMake(0, 0, self.authorViewContainer.frame.size.width, self.authorViewContainer.frame.size.height);
    [self.authorViewContainer addSubview:self.authorView];
    [self.authorView setup:[pullRequest author]];
}

@end
