//
//  PRListViewControllerDataSourceDelegate.h
//  DesafioConcrete
//
//  Created by Roger dos Santos Oliveira on 13/11/16.
//  Copyright © 2016 RogerSanoli. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol PRListViewControllerDataSourceDelegate <NSObject>
- (void) openURL:(NSString*) url;
@end
