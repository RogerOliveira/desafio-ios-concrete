//
//  PRDomainProtocol.h
//  DesafioConcrete
//
//  Created by Roger dos Santos Oliveira on 11/11/16.
//  Copyright © 2016 RogerSanoli. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RepoUserDomainProtocol.h"

@protocol PRDomainProtocol <NSObject>

- (NSString*) title;
- (NSString*) date;
- (NSString*) body;
- (NSString*) htmlURL;
- (BOOL) isOpen;
- (id<RepoUserDomainProtocol>) author;

- (void) prepareForTestWithDate: (NSString*) date;
- (void) prepareForTestWithState: (NSString*) state;

@end
