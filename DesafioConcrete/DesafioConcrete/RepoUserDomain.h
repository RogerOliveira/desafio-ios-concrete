//
//  RepoUserDomain.h
//  DesafioConcrete
//
//  Created by Roger dos Santos Oliveira on 11/11/16.
//  Copyright © 2016 RogerSanoli. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MTLModel.h"
#import "MTLJSONAdapter.h"
#import "NSValueTransformer+MTLPredefinedTransformerAdditions.h"
#import "RepoUserDomainProtocol.h"

@interface RepoUserDomain : MTLModel <RepoUserDomainProtocol, MTLJSONSerializing>

@property (nonatomic, strong) NSString* photoURL;
@property (nonatomic, strong) NSString* name;
@property (nonatomic, strong) NSString* fullName;

@end
