//
//  main.m
//  DesafioConcrete
//
//  Created by Roger dos Santos Oliveira on 10/11/16.
//  Copyright © 2016 RogerSanoli. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
