//
//  PRListViewControllerDataSource.m
//  DesafioConcrete
//
//  Created by Roger dos Santos Oliveira on 13/11/16.
//  Copyright © 2016 RogerSanoli. All rights reserved.
//

#import "PRListViewControllerDataSource.h"
#import "PRCell.h"

#define CELL_IDENTIFIER @"PRCell"

@interface PRListViewControllerDataSource ()
@property (nonatomic, strong) NSArray<id<PRDomainProtocol>>* prList;
@end

@implementation PRListViewControllerDataSource

#pragma mark - Initial setup & update
- (instancetype) init {
    self = [super init];
    if(self) {
    }
    return self;
}

- (void) updateDataSource:(NSArray<id<PRDomainProtocol>> *)prList {
    self.prList = prList;
}

#pragma mark TableView delegate & data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return nil == self.prList? 0 : [self.prList count];
}

- (UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath  {
    PRCell* cell = [tableView dequeueReusableCellWithIdentifier:CELL_IDENTIFIER];
    if(nil == cell) {
        cell = [[PRCell alloc] init];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    }
    
    id<PRDomainProtocol> currentPR = [self.prList objectAtIndex:[indexPath row]];
    [cell setup: currentPR];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(nil == self.delegate)
        return;
    
    id<PRDomainProtocol> currentPR = [self.prList objectAtIndex:[indexPath row]];
    [self.delegate openURL:[currentPR htmlURL]];
}

@end
